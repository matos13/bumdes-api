<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Pengguna extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_pengguna');
        $this->load->helper('string');
        date_default_timezone_set("Asia/Jakarta");

    }

    public function random_string($type = 'alnum', $len = 8)
    {
        switch ($type) {
            case 'basic':
                return mt_rand();
            case 'alnum':
            case 'numeric':
            case 'nozero':
            case 'alpha':
                switch ($type) {
                    case 'alpha':
                        $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'alnum':
                        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'numeric':
                        $pool = '0123456789';
                        break;
                    case 'nozero':
                        $pool = '123456789';
                        break;
                }
                return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
            case 'unique': // todo: remove in 3.1+
            case 'md5':
                return md5(uniqid(mt_rand()));
            case 'encrypt': // todo: remove in 3.1+
            case 'sha1':
                return sha1(uniqid(mt_rand(), true));
        }
    }

    public function raja_ongkir($request, $parameter){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/".$request."?".$parameter,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: 83a09db8abeafa00b2569d713890b0c7"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        return "cURL Error #:" . $err;
        } else {
        return $response;
        }
    }

    public function get_id_provinsi(){
        $request = "province";
        $parameter = "id=";
        return $this->raja_ongkir($request, $parameter);
    }

    public function kode_pos($id_provinsi){
        $request = "city";
        $parameter = "id="."&province=".$id_provinsi;
        return $this->raja_ongkir($request, $parameter);
    }

    public function index_get()
    {
        $id = $this->get('id');

        if ($id == '' || $id == null || $id == ' ') {
            $pengguna = $this->db->get('user_apk')->result_array();
            foreach($pengguna as $p){
                $p['provinsi']    = NULL;
                $p['id_provinsi'] = NULL;
                if(!empty($p['id_kelurahan'])){
                    $provinsi = $this->model_pengguna->provinsi_pengguna($p['id_kelurahan']);
                    if(!empty($provinsi)){
                        $p['id_provinsi'] = $provinsi['id_provinsi'];
                        $p['provinsi'] = $provinsi['provinsi'];
                        // $p['nama_kelurahan'] = $provinsi['kelurahan'];
                    }
                }
            }
        } else {
            $this->db->where('id', $id);
            $pengguna = $this->db->get('user_apk')->result_array();
            foreach($pengguna as $p){
                $p['provinsi']    = NULL;
                $p['id_provinsi'] = NULL;
                if(!empty($p['id_kelurahan'])){
                    $provinsi = $this->model_pengguna->provinsi_pengguna($p['id_kelurahan']);
                    if(!empty($provinsi)){
                        $p['id_provinsi'] = $provinsi['id_provinsi'];
                        $p['provinsi'] = $provinsi['provinsi'];
                        // $p['nama_kelurahan'] = $provinsi['kelurahan'];
                    }
                }
            }
        }

        if (!empty($pengguna)) {
            $pengguna = $p;
            $this->response(array(
                'status'  => true,
                'message' => 'Data pengguna ditemukan.',
                'data'    => array($pengguna),
            ), 200);

        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data pengguna tidak ditemukan.',
                'data'    => array(),
            ), 200);
        }
    }

    public function list_data_akun_usaha_get()
    {
        $id = $this->get('id');

        if (empty($id)) {
            $pengguna = $this->db->get('warung')->result_array();
        } else {
            $this->db->where('id', $id);
            $pengguna = $this->db->get('warung')->result_array();
        }

        if (!empty($pengguna)) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data warung ditemukan.',
                'data'    => $pengguna,
            ), 200);

        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data warung tidak ditemukan.',
                'data'    => array(),
            ), 200);
        }
    }


    public function detail_get()
    {
        $id = $this->get('id');
        if ($id == '') {
            $pengguna = $this->db->get('user_apk')->result();
        } else {
            $this->db->where('id', $id);
            $pengguna = $this->db->get('user_apk')->result();
        }
        if ($pengguna) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data available.',
                'data'    => $pengguna,
            ), 200);

        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data not available.',
                'data'    => array(),
            ), 200);
        }
    }

    public function daftar_post()
    {
        date_default_timezone_set('Asia/Jakarta');
        $trans_id    = date('this');
        $this->time  = date('Ymdhis');
        $token       = '3b22dcb855fc7d2441a57d85ea91a759';

        $phone         = $this->post('no_telp');
        if (empty($phone)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Nomor telepon tidak boleh kosong.',
                'data'    => array(),
            ), 200);
        }
        $id_kelurahan  = $this->post('id_kelurahan');
        $status        = $this->post('status');
        $created_at    = date('Y-m-d H:i:s');

        $firstNumber = substr($phone, 0, 1);
        if ($firstNumber != 0) {
            $send_to      = '0' . $phone;
            $phone_number = '0' . $phone;
            $otp_wa       = '62' . $phone;
        } else {
            $send_to      = $phone;
            $phone_number = $phone;
            $otp_wa       = substr($phone, 1, 20);
            $otp_wa       = '62' . $otp_wa;
        }

        // validasi inputan nomor handphone
        $telp = $this->model_pengguna->check_nomor($phone_number);

        if (!preg_match("/^[0-9|(\+|)]*$/", $phone)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Nomor telepon hanya boleh menggunakan angka.',
                'data'    => array(),
            ), 200);
        }else if ($telp != 0) {
            # code...
            $this->response(array(
                'status'  => false,
                'message' => 'Nomor telepon ('.$phone_number.') telah dipakai. Coba dengan nomor lain.',
                'data'    => array(),
            ), 200);
        } else {
            $auto_pass = 1234;
            $options   = [
                'cost' => 12,
            ];
            $password_hashed = password_hash($auto_pass, PASSWORD_BCRYPT, $options);

            $data = array(
                'USERNAME'          => $phone_number,
                'PASSWORD'          => $password_hashed,
                'NOHP'              => $phone_number,
                'STATUS'            => $status,
                'ID_KELURAHAN'      => $id_kelurahan,
                'CREATED_AT'        => $created_at,
            );

            // $insert = $this->db->insert('user_apk', $data);
            $insert = $this->model_pengguna->pengguna_insert($data);

            if ($insert) {

                $pesan = "*" . $auto_pass . "* adalah password Anda. Silakan melakukan pergantian demi keamanan data Anda. Terimakasih.";

                $kirim_pesan = kirim_pesan_wa_helper($pesan, $otp_wa);

                $this->response(array(
                    'status'  => true,
                    'message' => 'Pendaftaran berhasil dilakukan. Password akan di kirimkan ke nomor Whatsapp.',
                    'data'    => $insert,
                ), 200);
            } else {
                $this->response(array(
                    'status'  => false,
                    'message' => 'Pendaftaran gagal. Silakan mencoba kembali.',
                    'data'    => array(),
                ), 200);
            }
        }
    }

    public function data_diri_post()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id_user_apk    = $this->post('id_user_apk');
        $nik            = $this->post('nik');
        $jenis_kelamin  = $this->post('jenis_kelamin');
        $tempat_lahir   = $this->post('tempat_lahir');
        $tanggal_lahir  = $this->post('tanggal_lahir');
        $alamat_pemilik = $this->post('alamat_pemilik');
        $nama_pemilik   = $this->post('nama_pemilik');
        $foto_ktp       = $this->post('foto_ktp');
        $id_warung      = $this->post('id_warung');
        $created_at     = date('Y-m-d H:i:s');

        if (!empty($_FILES['foto_ktp']['name'])) {  
            $temp = $_FILES['foto_ktp']['tmp_name'];
            $filename = $_FILES['foto_ktp']['name'];
            $newname = rand(1,99999999);
            $newname = $newname . '.jpg';

            list($width_orig, $height_orig) = getimagesize($temp);
            $height_des = 1500;
            $width_des = ($height_des * $width_orig) / $height_orig;
            $image_p = imagecreatetruecolor($width_des, $height_des);
            $image = imagecreatefromjpeg($temp);
            imagecopyresampled($image_p, $image,0,0,0,0, $width_des, $height_des, $width_orig, $height_orig);
            if (file_exists("/home/wabumdes/public_html/public/images/profil/" . $newname)) {
                unlink("/home/wabumdes/public_html/public/images/profil/" . $newname);
            }
            imagejpeg($image, '/home/wabumdes/public_html/public/images/profil/' . $newname);
            $foto_ktp = 'http://wabumdes.com/public/images/profil/' . $newname;

            //base64 here        
            // $name    = $_FILES['foto_ktp']['tmp_name'];
            // $base_64 = base64_encode(file_get_contents($name));

        } else {
            $this->response(array(
                    'status'  => false,
                    'message' => 'Maaf, foto KTP tidak boleh kosong.',
                    'data'    => array(),
            ), 200);
        }

            $data = array(
                'ID_USER_APK'   => $id_user_apk,
                'NIK'           => $nik,
                'JENIS_KELAMIN' => $jenis_kelamin,
                'TEMPAT_LAHIR'  => $tempat_lahir,
                'TANGGAL_LAHIR'  => $tanggal_lahir,
                'ALAMAT_PEMILIK' => $alamat_pemilik,
                'NAMA_PEMILIK'  => $nama_pemilik,
                'FOTO_KTP'      => $foto_ktp,
                'ID_WARUNG'     => $id_warung,
                'CREATED_AT'    => $created_at,
            );

            $insert = $this->model_pengguna->data_diri_insert($data);

            if ($insert) {
                $this->response(array(
                    'status'  => true,
                    'message' => 'Terima Kasih telah mengisi dan mengirim data Anda. Silakan menunggu Verifikasi dari pihak WaBUMDES',
                    'data'    => $insert,
                ), 200);
            } else {
                $this->response(array(
                    'status'  => false,
                    'message' => 'Pendaftaran gagal. Silakan mencoba kembali.',
                    'data'    => array(),
                ), 200);
            }
        //}
    }

    public function data_warung_post()
    {
        date_default_timezone_set('Asia/Jakarta');

        //new params
        $warung         = $this->post('warung');
        $alamat         = $this->post('alamat');
        $id_grosir      = $this->post('id_grosir');
        $id_kelurahan   = $this->post('id_kelurahan');
        $nama_ktp       = $this->post('nama_ktp');
        $alamat_ktp     = $this->post('alamat_ktp');
        $no_hp          = $this->post('no_hp');

        $firstNumber = substr($no_hp, 0, 1);
        if ($firstNumber != 0) {
            $send_to      = '0' . $no_hp;
            $phone_number = '0' . $no_hp;
            $otp_wa       = '62' . $no_hp;
        } else {
            $send_to      = $no_hp;
            $phone_number = $no_hp;
            $otp_wa       = substr($no_hp, 1, 20);
            $otp_wa       = '62' . $otp_wa;
        }

        $id_user_apk    = $this->post('id_user_apk');
        $nik            = $this->post('nik');
        $jenis_kelamin  = $this->post('jenis_kelamin');
        $tempat_lahir   = $this->post('tempat_lahir');
        $tanggal_lahir  = $this->post('tanggal_lahir');
        $alamat_pemilik = $this->post('alamat_pemilik');
        $nama_pemilik   = $this->post('nama_pemilik');
        $ktp            = $this->post('foto_ktp');
        $created_at     = date('Y-m-d H:i:s');

        if (!empty($_FILES['foto_ktp']['name'])) {  
            $temp = $_FILES['foto_ktp']['tmp_name'];
            $filename = $_FILES['foto_ktp']['name'];
            $newname = rand(1,99999999);
            $newname = $newname . '.jpg';

            list($width_orig, $height_orig) = getimagesize($temp);
            $height_des = 1500;
            $width_des = ($height_des * $width_orig) / $height_orig;
            $image_p = imagecreatetruecolor($width_des, $height_des);
            $image = imagecreatefromjpeg($temp);
            imagecopyresampled($image_p, $image,0,0,0,0, $width_des, $height_des, $width_orig, $height_orig);
            if (file_exists("/home/wabumdes/public_html/public/images/profil/" . $newname)) {
                unlink("/home/wabumdes/public_html/public/images/profil/" . $newname);
            }
            imagejpeg($image, '/home/wabumdes/public_html/public/images/profil/' . $newname);
            $ktp = 'http://wabumdes.com/public/images/profil/' . $newname;

        } else {
            $this->response(array(
                    'status'  => false,
                    'message' => 'Maaf, foto KTP tidak boleh kosong.',
                    'data'    => array(),
            ), 200);
        }

            $data = array(
                'ID_USER_APK'   => $id_user_apk,
                'NIK'           => $nik,
                'JENIS_KELAMIN' => $jenis_kelamin,
                'TEMPAT_LAHIR'  => $tempat_lahir,
                'TGL_LAHIR'     => $tanggal_lahir,
                'ALAMAT_PEMILIK' => $alamat_pemilik,
                'NAMA_PEMILIK'  => $nama_pemilik,
                'KTP'           => $ktp,
                'CREATED_AT'    => $created_at,
                'WARUNG'        => $warung,
                'ALAMAT'        => $alamat,
                'ID_GROSIR'     => $id_grosir,
                'ID_KELURAHAN'  => $id_kelurahan,
                'NAMA_KTP'      => $nama_ktp,
                'ALAMAT_KTP'    => $alamat_ktp,
                'NO_HP'         => $no_hp,
            );

            $insert = $this->model_pengguna->data_warung_insert($data);

            $pesan = "Ini adalah pesan otomatis dari WaBUMDES. Berikut adalah data terkait Data Warung yang Anda inputkan.\n\n";
            $pesan .= "Nama Pemilik : *" . $nama_pemilik . "*.\n";
            $pesan .= "Alamat Pemilik : *" . $alamat_pemilik . "*.\n";
            $pesan .= "Nama Warung : *" . $warung . "*.\n";
            $pesan .= "Alamat Warung : *" .$alamat . "*.\n";
            $kirim_pesan = kirim_pesan_wa_helper($pesan, $otp_wa);

            if ($insert) {
                $this->response(array(
                    'status'  => true,
                    'message' => 'Terima Kasih telah mengisi dan mengirim data Anda. Selanjutnya akun aktif bisa digunakan untuk Login Aplikasi.',
                    'data'    => $insert,
                ), 200);
            } else {
                $this->response(array(
                    'status'  => false,
                    'message' => 'Pendaftaran gagal. Silakan mencoba kembali.',
                    'data'    => array(),
                ), 200);
            }
        //}
    }

    // update data pengguna master
    public function index_put()
    {
        $id            = $this->put('id');
        $phone         = $this->put('no_telp');
        $id_kelurahan  = $this->put('id_kelurahan');
        $status        = $this->put('status');

        $firstNumber = substr($phone, 0, 1);
        if ($firstNumber != 0) {
            $send_to      = '0' . $phone;
            $phone_number = '0' . $phone;
        } else {
            $send_to      = $phone;
            $phone_number = $phone;
        }

        if (!preg_match("/^[0-9|(\+|)]*$/", $phone_number)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Nomor telepon hanya boleh menggunakan angka.',
                'data'    => array(),
            ), 200);
        }

        $data = array(
            'id'                => $id,
            'username'          => $phone_number,
            'nohp'              => $phone_number,
            'id_kelurahan'      => $id_kelurahan,
            'status'            => $status,
        );

        //update disini menggunakan Body > x-www-form-url-encoded
        $update = $this->model_pengguna->update(array('id' => $id), $data);

        $this->db->where('id', $id);
        $data = $this->db->get('user_apk')->row_array();
        
        // foreach($data as $p){
            $data['provinsi']    = NULL;
            $data['id_provinsi'] = NULL;
            if(!empty($data['id_kelurahan'])){
                $provinsi = $this->model_pengguna->provinsi_pengguna($data['id_kelurahan']);
                if(!empty($provinsi)){
                    $data['id_provinsi'] = $provinsi['id_provinsi'];
                    $data['provinsi'] = $provinsi['provinsi'];
                }
            }
        // }


        if ($update) {
            $this->response([
                'status'  => true,
                'message' => 'Profile pengguna berhasil di update.',
                'data'    => array($data),
            ], 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Profile pengguna sudah/gagal di update. Silakan mencoba kembali dengan merubah data pada profile Anda.',
                'data'    => array(),
            ), 200);
        }
    }

    // update data pengguna
    public function updatepassword_put()
    {
        $id_pengguna_app = $this->put('id_pengguna');
        $old_password    = $this->put('password_lama');
        $new_password    = $this->put('password_baru');

        $options = [
            'cost' => 12,
        ];

        $password_hashed = password_hash($new_password, PASSWORD_BCRYPT, $options);

        $pengguna_app = $this->model_pengguna->get_by_($id_pengguna_app);

        if (password_verify($old_password, $pengguna_app['password'])) {
            $data = [
                'password' => $password_hashed,
            ];
            $updated_data = $this->model_pengguna->update(array('id' => $id_pengguna_app), $data);
            if ($updated_data) {
                $this->response([
                    'status'  => true,
                    'message' => 'Password berhasil diganti',
                    'data'    => [$data],
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status'  => false,
                    'message' => 'Password gagal diganti',
                    'data'    => [],
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status'  => false,
                'message' => 'Password lama anda tidak sesuai, mohon periksa kembali.',
                'data'    => [],
            ], REST_Controller::HTTP_OK);
        }

    }

    // update data pengguna
    public function firebase_put()
    {
        $id          = $this->put('id_pengguna');
        $id_firebase = $this->put('id_firebase');

        $data = array(
            'id_firebase' => $id_firebase,
        );

        //update disini menggunakan Body > x-www-form-url-encoded
        $update = $this->model_pengguna->update(array('id' => $id), $data);

        if ($update) {
            $this->response([
                'status'  => true,
                'message' => 'Data firebase berhasil diupdate',
                'data'    => array($data),
            ], 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data firebase gagal diupdate',
                'data'    => array(),
            ), 200);
        }
    }

    public function notification_firebase_get()
    {
        $id       = $this->get('id_pengguna');
        $firebase = $this->model_pengguna->get_firebase($id);

        if ($firebase['id_firebase'] == '' || $firebase['id_firebase'] == null) {
            $this->response(array(
                'status'  => false,
                'message' => 'Error. Id Firebase kosong.',
                'data'    => array(),
            ), 200);
        }

        $url   = "https://fcm.googleapis.com/fcm/send";
        $token = "$firebase[id_firebase]";

        $serverKey = 'AAAAkobK6qQ:APA91bEW_T6jGgtxhaDgLDgbbOKBBXNHMfdOmqqvcUAcL4UTEVeekBxR3-vHARGTi3W0Fk0gskyRIyLNJs_91PEvNyRF2hRLKVVgnoYopitvE53_rBaRVh5RI5849sn06cNPWIlQBg3W';

        $title       = "Ini notifikasi";
        $body        = "Ini body notifikasi";
        $data        = array('title' => $title, 'message' => $body, 'content' => '');
        $arrayToSend = array('to' => $token, 'data' => $data);

        $json      = json_encode($arrayToSend);
        $headers   = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $json,
            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->response(array(
                'status'  => false,
                'message' => 'Error. Gagal mengirim kode notifikasi.',
                'data'    => array(),
            ), 200);

        } else {
            $get_response = json_decode($response);

            if ($get_response->success == '1') {
                $this->response([
                    'to'   => $token,
                    'data' => $data,
                ], 200);
            } else {
                $this->response(array(
                    'status'  => false,
                    'message' => 'Gagal mengirim response notifikasi. Id Firebase tidak valid.',
                    'data'    => array(),
                ), 200);
            } //$get_response->status == '0'

        }
    }

    public function resetpassword_put()
    {

        date_default_timezone_set('Asia/Jakarta');
        $phone = $this->put('nohp');

        $cek_telp = $this->model_pengguna->check_nomor($phone);
        if ($cek_telp == 0) {

            $this->response(array(
                'status'  => false,
                'message' => 'Maaf nomor yang anda masukkan salah atau tidak terdaftar pada sistem kami.',
                'data'    => array(),
            ), 200);
        }

        $firstNumber = substr($phone, 0, 1);
        if ($firstNumber != 0) {
            $send_to      = '0' . $phone;
            $phone_number = '0' . $phone;
            $otp_wa       = '62' . $phone;
        } else {
            $send_to      = $phone;
            $phone_number = $phone;
            $otp_wa       = substr($phone, 1, 20);
            $otp_wa       = '62' . $otp_wa;
        }

        // $auto_pass = 1234;
        $auto_pass = random_string('numeric', 4);

        $options = [
            'cost' => 12,
        ];

        $password_hashed = password_hash($auto_pass, PASSWORD_BCRYPT, $options);

        $data = array(
            'PASSWORD' => $password_hashed,
        );

        //update disini menggunakan Body > x-www-form-url-encoded
        $update = $this->model_pengguna->update(array('USERNAME' => $phone), $data);

        $data['pass'] = $auto_pass;

        $pesan = "*" . $auto_pass . "* adalah password Anda. Silakan melakukan pergantian demi keamanan data Anda. Terimakasih.";

        if ($update) {

            $kirim_pesan = kirim_pesan_wa_helper($pesan, $otp_wa);

            $this->response([
                'status'  => true,
                'message' => 'Password berhasil di reset. ' . $auto_pass . ' adalah password baru anda',
                'data'    => array($data),
            ], 200);

        } else {

            $this->response(array(
                'status'  => false,
                'message' => 'Gagal reset password.',
                'data'    => array(),
            ), 200);

        }

    }


}
