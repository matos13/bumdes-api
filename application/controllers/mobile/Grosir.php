<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Grosir extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_grosir');
        $this->load->helper('string');
        date_default_timezone_set("Asia/Jakarta");

    }

    public function insert_post()
    {
        date_default_timezone_set('Asia/Jakarta');

        $grosir         = $this->post('grosir');
        $id_kelurahan   = $this->post('id_kelurahan');
        $alamat         = $this->post('alamat');
        $flag_aktif     = $this->post('flag_aktif');
        $created_at     = date('Y-m-d H:i:s');

            $data = array(
                'GROSIR'   => $grosir,
                'ID_KELURAHAN' => $id_kelurahan,
                'ALAMAT' => $alamat,
                'FLAG_AKTIF'  => $flag_aktif,
                'CREATED_AT'    => $created_at,
            );

            $insert = $this->db->insert('grosir', $data);

            if ($insert) {
                $this->response(array(
                    'status'  => true,
                    'message' => 'Grosir berhasil disimpan.',
                    'data'    => array($data),
                ), 200);
            } else {
                $this->response(array(
                    'status'  => false,
                    'message' => 'Grosir gagal disimpan.',
                    'data'    => array(),
                ), 200);
            }
    }

    public function list_get()
    {
        $list = $this->model_grosir->get_data();

        if ($list) {
            $this->response(array(
                'status'  => true,
                'message' => 'Grosir tersedia.',
                'data'    => $list,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Grosir tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

    public function list_skema_get()
    {
        $id_barang = $this->get('id_barang');
        $list = $this->model_grosir->get_data_skema($id_barang);

        if ($list) {
            $this->response(array(
                'status'  => true,
                'message' => 'Grosir tersedia.',
                'data'    => $list,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Grosir tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }


}
