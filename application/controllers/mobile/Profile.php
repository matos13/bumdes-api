<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Profile extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Model_profile');
    }

    public function kelurahan_get(){
        $id_kecamatan = $this->get('id_kecamatan');
        $provinsi = $this->get('provinsi');
        

        if(!empty($provinsi)){
            $data = $this->Model_profile->get_kelurahan_detail($provinsi);
            if(!empty($data)){
                $this->response(array(
                    'status'  => true,
                    'message' => 'data kelurahan ditemukan.',
                    'data'    => $data,
                ), REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                    'status'  => false,
                    'message' => 'data kelurahan tidak ditemukan.',
                    'data'    => array(),
                ), REST_Controller::HTTP_NOT_FOUND);
            }
        }else{
            $data = $this->Model_profile->get_kelurahan();
            if(!empty($data)){
                $this->response(array(
                    'status'  => true,
                    'message' => 'data kelurahan ditemukan.',
                    'data'    => $data,
                ), REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                    'status'  => false,
                    'message' => 'data kelurahan tidak ditemukan.',
                    'data'    => array(),
                ), REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function kelurahan_v2_get()
    {
        $nama_kelurahan = $this->get('nama_kelurahan');

        $data = $this->Model_profile->get_kelurahan_by_nama_kelurahan($nama_kelurahan);

        $list_kelurahan = [];
        foreach ($data as $key => $value) {
            $value['nama'] = $value['nama_kelurahan'].', '.$value['nama_kecamatan'].', '.$value['nama_kabupaten'].', '.$value['nama_provinsi'];
            $list_kelurahan[]       = $value;
        }
        $data = $list_kelurahan;

        if (!empty($data)) {
            $this->response(array(
                'status'  => true,
                'message' => 'data kelurahan ditemukan.',
                'data'    => $data,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'data kelurahan tidak ditemukan.',
                'data'    => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

}