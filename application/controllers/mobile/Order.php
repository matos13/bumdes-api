<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Order extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Model_order');
        $this->load->helper('string');
        date_default_timezone_set("Asia/Jakarta");

    }

    public function list_get()
    {
        $id_user = $this->get('id_user');
        $order_data = $this->Model_order->order_v2_get($id_user);

        if ($order_data) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data order berhasil di proses.',
                'data'    => $order_data,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data order gagal di proses.',
                'data'    => array(),
            ), 200);
        }
    }

    public function insert_post()
    {   
        $post = $this->post();
        $input_order = $post['order'];
        $insert_order = $this->insert_order($input_order);

        if ($insert_order) {
            $this->response(array(
                'status'  => true,
                'message' => 'Order barang berhasil di proses.',
                'data'    => array($insert_order),
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Order barang gagal di proses.',
                'data'    => array(),
            ), 200);
        }
    }

    public function insert_order($input_order)
    {   
        $this->db->trans_start();
        date_default_timezone_set('Asia/Jakarta');
        $created_at = date('Y-m-d H:i:s');
        
        // looping berdasarkan jumlah id_grosir yang di muat
        // grouping data grosir disini
        $result = [];
        foreach ($input_order as $values) {
            if (!array_key_exists($values['id_grosir'], $result)) {
                $result[$values['id_grosir']] = array(
                    'id_user_apk'  => $values['id_user_apk'],
                    'status_order' => $values['status_order'],
                    'tanggal_order' => $values['tanggal_order'],
                    'latitude' => $values['latitude'],
                    'longitude' => $values['longitude'],
                    'total' => $values['total'],
                    'id_grosir' => $values['id_grosir'],
                );

                $result[$values['id_grosir']]['detail'] = [];
            }

            $result[$values['id_grosir']]['detail'][] = $values;
        }

        //insert data order disini
        foreach ($result as $key => $value) {
            $nomor_order = $this->Model_order->nomor_order();
            $data_order = array(
                'id_user_apk'  => $value['id_user_apk'],
                'status_order' => $value['status_order'],
                'tanggal_order' => $value['tanggal_order'],
                'latitude' => $value['latitude'],
                'longitude' => $value['longitude'],
                'total' => $value['total'],
                'created_at' => $created_at,
                'updated_at' => $created_at,
                'nomor_order' => $nomor_order,
                'id_grosir' => $value['id_grosir'],
            );

            $insert_oder = $this->Model_order->order_insert($data_order);
            $id_order = $insert_oder[0]['id'];

            // insert data detail order
            foreach ($value['detail'] as $keys => $valuez) {
                $data_detail_order = array(
                    'id_barang' => $valuez['id_barang'],
                    'id_order' => $id_order,
                    'qty' => $valuez['qty'],
                    'harga' => $valuez['harga'],
                    'subtotal' => $valuez['subtotal'],
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );
                $detail_insert_oder = $this->Model_order->detail_order_insert($data_detail_order);
            } // end detail

        } //end order

        $this->db->trans_complete();

        return $data_order;
    }

}
