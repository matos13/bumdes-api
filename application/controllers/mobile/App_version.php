<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class App_version extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_app_version');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index_get()
    {
        $app_version = $this->model_app_version->app_version_get();

        if ($app_version) {
            $this->response(array(
                'status'  => true,
                'message' => 'App version tersedia.',
                'data'    => $app_version,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'App version tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

    public function index_put()
    {
        $id = $this->put('id');
        $check_id = $this->put('check_id');

        $app_version = $this->model_app_version->app_version_get($id);
        $id          = $app_version[0]['id'];

        if (!empty($check_id)) {
            $this->response([
                'status'  => true,
                'message' => 'App version berhasil di-check.',
                'data'    => array($app_version[0]),
            ], 200);
        }

        $version     = $this->put('version');

        $data = array(
            'version' => $version,
        );

        //update disini menggunakan Body > x-www-form-url-encoded
        $update = $this->model_app_version->update(array('id' => $id), $data);

        if ($update) {
            $this->response([
                'status'  => true,
                'message' => 'App version berhasil di-update.',
                'data'    => array($data),
            ], 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'App version gagal di-update. Coba lagi.',
                'data'    => array(),
            ), 200);
        }
    }

}
