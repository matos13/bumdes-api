<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Barang extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_barang');
        $this->load->model('model_grosir');
        $this->load->model('model_kategori_barang');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index_get()
    {
        $id_barang = $this->get('id_barang');
        $Barang = $this->model_barang->get_data($id_barang);

        foreach ($Barang as $key => $value) {
            $id_barang_inskema = $value['id'];
            $Skema_grosir = $this->model_grosir->get_data_skema($id_barang_inskema);
            
            if (!empty($value['gambar'])) {
                $base_image = 'http://wabumdes.com/public/images/barang/';
                $value['gambar'] = $base_image.$value['gambar'];
            } else {
                $base_image = '';
                $value['gambar'] = null;
            }

                //wajib deklarasi array di luar foreach
                $List_skema_grosir = [];
                foreach ($Skema_grosir as $key => $skb) {
                    $List_skema_grosir[] = $skb;
                }

                $features[] = (object)[
                "Barang" => [$value],
                "Skema_grosir" => $List_skema_grosir,
                ];
        }

        if ($features) {
            $this->response(array(
                'status'  => true,
                'message' => 'Barang tersedia.',
                'data'    => $features,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Barang tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

    public function search_by_nama_barang_get()
    {
        $nama_barang = $this->get('nama_barang');
        $Barang = $this->model_kategori_barang->get_data_by_name($nama_barang);

        foreach ($Barang as $key => $value) {
            $id_barang_inskema = $value['id'];
            $Skema_grosir = $this->model_grosir->get_data_skema($id_barang_inskema);
            
            if (!empty($value['gambar'])) {
                $base_image = 'http://wabumdes.com/public/images/barang/';
                $value['gambar'] = $base_image.$value['gambar'];
            } else {
                $base_image = '';
                $value['gambar'] = null;
            }

                //wajib deklarasi array di luar foreach
                $List_skema_grosir = [];
                foreach ($Skema_grosir as $key => $skb) {
                    $List_skema_grosir[] = $skb;
                }

                $features[] = (object)[
                "Barang" => [$value],
                "Skema_grosir" => $List_skema_grosir,
                ];
        }

        if ($features) {
            $this->response(array(
                'status'  => true,
                'message' => 'Barang tersedia.',
                'data'    => $features,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Barang tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

    public function barang_by_id_sub_kategori_get()
    {
        $id_sub_kategori = $this->get('id_sub_kategori');
        $Barang = $this->model_kategori_barang->get_data_sub_barang($id_sub_kategori);
        
        foreach ($Barang as $key => $value) {
            $id_barang_inskema = $value['id'];
            $Skema_grosir = $this->model_grosir->get_data_skema($id_barang_inskema);
            
            if (!empty($value['gambar'])) {
                $base_image = 'http://wabumdes.com/public/images/barang/';
                $value['gambar'] = $base_image.$value['gambar'];
            } else {
                $base_image = '';
                $value['gambar'] = null;
            }

                //wajib deklarasi array di luar foreach
                $List_skema_grosir = [];
                foreach ($Skema_grosir as $key => $skb) {
                    $List_skema_grosir[] = $skb;
                }

                $features[] = (object)[
                "Barang" => [$value],
                "Skema_grosir" => $List_skema_grosir,
                ];
        }

        if ($features) {
            $this->response(array(
                'status'  => true,
                'message' => 'Barang tersedia.',
                'data'    => $features,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Barang tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

}
