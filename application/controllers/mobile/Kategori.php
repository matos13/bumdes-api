<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Kategori extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_kategori_barang');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function semua_kategori_get()
    {
        $Kategori_barang = $this->model_kategori_barang->get_data();

        foreach ($Kategori_barang as $key => $kb) {
            $id_kategori = $kb['id'];
            $sub_kategori_barang = $this->model_kategori_barang->get_data_sub_kategori($id_kategori);

                if (!empty($kb['icon'])) {
                    $base_image = 'http://wabumdes.com/public/images/kategori-barang-icon/';
                    $kb['icon'] = $base_image.$kb['icon'];
                } else {
                    $base_image = '';
                    $kb['icon'] = null;
                }

                //wajib deklarasi array di luar foreach
                $list_sub_kategori_barang = [];
                foreach ($sub_kategori_barang as $key => $skb) {

                        if (!empty($skb['icon'])) {
                            $base_image = 'http://wabumdes.com/public/images/sub-kategori-barang-icon/';
                            $skb['icon'] = $base_image.$skb['icon'];
                        } else {
                            $base_image = '';
                            $skb['icon'] = null;
                        }
                    $list_sub_kategori_barang[] = $skb;
                }

                $features[] = (object)[
                "Kategori" => [$kb],
                "Sub_Kategori" => $list_sub_kategori_barang,
                ];
        }

        //set the format json here

        if ($features) {
            $this->response(array(
                'status'  => true,
                'message' => 'Semua Kategori Barang tersedia.',
                'data'    => $features,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Semua Kategori Barang tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

    public function kategori_barang_get()
    {
        $id_kategori = $this->get('id_kategori');
        $Kategori_barang = $this->model_kategori_barang->get_data($id_kategori);

        foreach ($Kategori_barang as $key => $value) {
            if (!empty($value['icon'])) {
                $base_image = 'http://wabumdes.com/public/images/kategori-barang-icon/';
                $value['icon'] = $base_image.$value['icon'];
            } else {
                $base_image = '';
                $value['icon'] = null;
            }
            $list_kategori_barang[] = $value;
        }

        if ($Kategori_barang) {
            $this->response(array(
                'status'  => true,
                'message' => 'Kategori Barang tersedia.',
                'data'    => $list_kategori_barang,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Kategori Barang tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

    public function sub_kategori_barang_get()
    {
        $id_kategori = $this->get('id_kategori');
        $Kategori_barang = $this->model_kategori_barang->get_data_sub_kategori($id_kategori);

        foreach ($Kategori_barang as $key => $value) {
            $id_kategori = $value['id_kategori_barang'];
            $list_ = $this->model_kategori_barang->get_data($id_kategori);

            if (!empty($value['icon'])) {
                $base_image = 'http://wabumdes.com/public/images/sub-kategori-barang-icon/';
                $value['icon'] = $base_image.$value['icon'];
            } else {
                $base_image = '';
                $value['icon'] = null;
            }
            $list_kategori_barang[] = $value;

            $features = (object)[
                "Kategori" => $list_,
                "Sub_Kategori" => $list_kategori_barang,
            ];

        }

        if ($Kategori_barang) {
            $this->response(array(
                'status'  => true,
                'message' => 'Sub Kategori Barang tersedia.',
                'data'    => [$features],
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Sub Kategori Barang tidak tersedia.',
                'data'    => array(),
            ), 200);
        }
    }

}
