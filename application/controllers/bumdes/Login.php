<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Login extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_login');
        date_default_timezone_set("Asia/Jakarta");
    }
    public function index_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');
        $status_login = $this->post('status_login');

        $pengguna = $this->model_login->get_by_username_bumdes($username);

        if (!empty($pengguna)) {

                $id_pengguna = $pengguna['id'];

                $data = [
                    'username' => $username,
                    'password' => $password,
                    'status_login' => $status_login,
                ];

                $pengguna_true = $this->model_login->login($data);

                if ($pengguna_true) {
                    $this->response([
                        'status'  => true,
                        'message' => 'Login berhasil.',
                        'data'    => array($pengguna),
                    ], 200);
                } else {
                    $this->response([
                        'status'  => false,
                        'message' => 'Username atau Password salah.',
                        'data'    => array(),
                    ], 200);
                }
            
        } else {
            $this->response([
                'status'  => false,
                'message' => 'Pengguna dengan nomor telp ' . $username . ' tidak ditemukan. Silakan melakukan registrasi terlebih dahulu. ',
                'data'    => array(),
            ], 200);
        }
    }
    
}
