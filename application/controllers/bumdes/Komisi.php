<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Komisi extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Model_komisi');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function insert_komisi_post()
    {   
        $id_order = $this->post('id_order');
        $id_bumdes = $this->post('id_bumdes');
        $total_belanja = $this->post('total_belanja');

        // hitung di sini untuk komisi
        $presentase_komisi = 5/100;
        $total_komisi = $presentase_komisi * $total_belanja;
        $created_at     = date('Y-m-d H:i:s');

        $data_komisi = array(
                'id_order'  => $id_order,
                'id_bumdes' => $id_bumdes,
                'total_belanja' => $total_belanja,
                'presentase_komisi' => $presentase_komisi,
                'total_komisi' => $total_komisi,
                'tanggal_komisi' => $created_at,
                'status_bayar' => 'N',
                'created_at' => $created_at,
                'updated_at' => $created_at
            );

        $insert_order = $this->Model_komisi->komisi_insert($data_komisi);

        if ($insert_order) {
            $this->response(array(
                'status'  => true,
                'message' => 'Komisi Penjualan berhasil di proses.',
                'data'    => $insert_order,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Komisi Penjualan gagal di proses.',
                'data'    => array(),
            ), 200);
        }
    }

    public function history_get()
    {
        $id_bumdes = $this->get('id_bumdes');

        if (empty($id_bumdes)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Maaf, id bumdes tidak boleh kosong, silakan login sesuai akun bumdes yang ada.',
                'data'    => array(),
            ), 200);
        }


        $data_pembayaran = $this->Model_komisi->get_data_komisi_history($id_bumdes);

        if (!empty($data_pembayaran)) {
            for ($x = 0; $x < count($data_pembayaran); $x++) {
                if (isset($data_pembayaran[$x]['created_at'])) {
                    $data_pembayaran[$x]['tanggal_komisi']= date('d F Y', strtotime($data_pembayaran[$x]['created_at']));
                }
            }
        }

        if ($data_pembayaran) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data Histori Komisi tersedia.',
                'data'    => $data_pembayaran,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data Histori Komisi tidak tersedia.',
                'data'    => array(),
            ), 200);
        }

    }

    
    
}
