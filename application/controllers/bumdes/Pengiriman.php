<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Pengiriman extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('model_pembayaran');
        $this->load->model('model_order');
        $this->load->model('model_log_order');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function list_get()
    {
        $status = $this->get('status');
        $id_user_apk = $this->get('id_user_apk');
        if (empty($status)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Maaf, status tidak boleh kosong, silakan isi sesuai status yang ada.',
                'data'    => array(),
            ), 200);
        }

        if (empty($id_user_apk)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Maaf, id user tidak boleh kosong, silakan login sesuai akun bumdes yang ada.',
                'data'    => array(),
            ), 200);
        }

        $data_pembayaran = $this->model_pembayaran->get_data($status, $id_user_apk);
        if ($data_pembayaran) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data Pengiriman tersedia.',
                'data'    => $data_pembayaran,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data Pengiriman tidak tersedia.',
                'data'    => array(),
            ), 200);
        }

    }

    public function list_detail_get()
    {
        $id_order = $this->get('id_order');

        if (empty($id_order)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Maaf, id order tidak boleh kosong.',
                'data'    => array(),
            ), 200);
        }
        
        $data_pembayaran = $this->model_pembayaran->get_data_detail($id_order);
        if ($data_pembayaran) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data Pengiriman tersedia.',
                'data'    => $data_pembayaran,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data Pengiriman tidak tersedia.',
                'data'    => array(),
            ), 200);
        }

    }

    public function pengiriman_order_warung_post()
    {   
        $post = $this->post();
        $input_order = $post['order'];
        $insert_order = $this->pengiriman_order_($input_order);

        if ($insert_order) {
            $this->response(array(
                'status'  => true,
                'message' => 'Pengiriman barang berhasil di proses.',
                'data'    => array($insert_order),
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Pengiriman barang gagal di proses.',
                'data'    => array(),
            ), 200);
        }
    }

    public function pengiriman_order_($input_order)
    {   
        $this->db->trans_start();
        date_default_timezone_set('Asia/Jakarta');
        $created_at = date('Y-m-d H:i:s');

        //insert data order disini
        foreach ($input_order as $key => $value) {
            $data_order = array(
                'status_order' => 'pengiriman_pesanan',
                'updated_at' => $created_at
            );

            $where = [
                'id' => $value['id'],
            ];

            $insert_oder = $this->model_order->update_order_from_bumdes($where, $data_order);

            // insert log here 
            $data_order_log = array(
                'id_order' => $value['id'],
                'status' => 'pengiriman_pesanan',
                'created_at' => $created_at,
                'updated_at' => $created_at
            );

            $data_log = $this->model_log_order->get_data_konfirmasi_by_id($value['id'],'pengiriman_pesanan');

            if (empty($data_log)) {
                $insert_log = $this->model_order->insert_log_order($data_order_log);
            }

        } //end order

        $this->db->trans_complete();

        return $data_order;
    }

    public function history_get()
    {
        $status = $this->get('status');
        $id_user_apk = $this->get('id_user_apk');
        if (empty($status)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Maaf, status tidak boleh kosong, silakan isi sesuai status yang ada.',
                'data'    => array(),
            ), 200);
        }

        if (empty($id_user_apk)) {
            $this->response(array(
                'status'  => false,
                'message' => 'Maaf, id user tidak boleh kosong, silakan login sesuai akun bumdes yang ada.',
                'data'    => array(),
            ), 200);
        }

        $data_pembayaran = $this->model_log_order->get_data($status, $id_user_apk);

        if (!empty($data_pembayaran)) {
            for ($x = 0; $x < count($data_pembayaran); $x++) {
                if (isset($data_pembayaran[$x]['created_at'])) {
                    $data_pembayaran[$x]['tanggal_pengiriman']= date('d F Y', strtotime($data_pembayaran[$x]['created_at']));
                }
            }
        }

        if ($data_pembayaran) {
            $this->response(array(
                'status'  => true,
                'message' => 'Data Histori Pengiriman tersedia.',
                'data'    => $data_pembayaran,
            ), 200);
        } else {
            $this->response(array(
                'status'  => false,
                'message' => 'Data Histori Pengiriman tidak tersedia.',
                'data'    => array(),
            ), 200);
        }

    }

    
    
}
