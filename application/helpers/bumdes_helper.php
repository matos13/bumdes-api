<?php

defined('BASEPATH') or exit('No direct script access allowed');

function set_time_zone() {
    $time = date_default_timezone_set('Asia/Jakarta');
    return $time;
}

function kirim_pesan_wa_helper($pesan, $no_telp){

    // TOKEN : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzAyOTMwODYsInVzZXIiOiI2Mjg1NjQwOTYzMDg5In0.A2W62wS2Lxs5gUgdqWt73fqLLjktqlSCydvJ1SYKobc
    // ID : 04447c77-b83c-4b1b-a59b-9959dacaf75e

    $chatApiToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzExNTc3NzMsInVzZXIiOiI2Mjg1NjQwOTYzMDg5In0.LtdzJYlaOA3xxsEyzkCq5_Wi6KbyxJFIGPysT4lSe70";

    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://chat-api.phphive.info/message/send/text',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>json_encode(array("jid"=> $no_telp."@s.whatsapp.net", "message" => $pesan)),
    CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer '.$chatApiToken,
    'Content-Type: application/json'
    ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}