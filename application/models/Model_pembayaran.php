<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_pembayaran extends CI_model
{
    public function get_data($status=null,$id_user_apk=null)
    {
        $query = $this->db->select('od.*, g.grosir, wr.warung as nama_warung, wr.alamat as alamat_warung, wr.nama_pemilik as nama_pemilik_warung, wr.alamat_pemilik as alamat_pemilik_warung, wr.id as id_warung')
        ->from('order_data od')
        ->join('grosir g','od.id_grosir=g.id')
        ->join('warung wr','wr.id_grosir=g.id')
        ->where('od.status_order',$status)
        ->where('od.id_user_apk',$id_user_apk)
        ->order_by('od.created_at','desc');
        return $query->get()->result_array();
    }

    public function get_data_detail($id_order)
    {
        $query = $this->db->select('*')
        ->from('order_data o')
        ->where('o.id',$id_order)
        ->group_by('o.id')
        ->order_by('o.created_at','desc');
        
        $data = $query->get()->result_array();
        foreach ($data as $key => $value) {
            $query = $this->db->select('d.*,b.barang, skb.id as id_sub_kategori_barang,skb.sub_kategori_barang,kb.id as id_kategori_barang, kb.kategori_barang')
            ->from('order_data o')
            ->join('detail_order d','o.id=d.id_order')
            ->join('barang b','b.id=d.id_barang')
            ->join('sub_kategori_barang skb','b.id_sub_kategori_barang=skb.id')
            ->join('kategori_barang kb','skb.id_kategori_barang=kb.id')
            ->where('d.id_order',$value['id']);
            $detail = $query->get()->result_array();
            $data[$key]['detail_order'] = $detail;
            $data[$key]['jumlah_barang'] = count($detail);
        }
        return $data;
    }

}
