<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_profile extends CI_Model
{
    public function get_kelurahan_by_nama_kelurahan($nama_kelurahan)
    {
        $nama_kelurahan = strtolower($nama_kelurahan);

        $sql = $this->db->query("SELECT kel.kelurahan as nama_kelurahan, kec.kecamatan as nama_kecamatan, kab.kabupaten as nama_kabupaten, prov.provinsi as nama_provinsi, kel.id as id from kelurahan kel
        join kecamatan kec on kec.id = kel.id_kecamatan
        join kabupaten kab on kab.id = kec.id_kabupaten
        join provinsi prov on prov.id = kab.id_provinsi
        where lower(kel.kelurahan) like '%$nama_kelurahan%'");

        return $sql->result_array();
    }

}
