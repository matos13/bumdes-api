<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_komisi extends CI_model
{
    public function komisi_insert($data)
    {
        $this->db->insert('komisi', $data);
        $success = $this->db->affected_rows();
        if ($success) {
            $insert_id = $this->db->insert_id();
            $data      = $this->db->get_where('komisi', array('id' => $insert_id))->result_array();
            return $data;
        }
    }

    public function get_data_komisi_history($id_bumdes=null)
    {
        $query = $this->db->select('*')
        ->from('komisi k')
        ->where('k.id_bumdes',$id_bumdes)
        ->order_by('k.created_at','desc');
        return $query->get()->result_array();
    }

    public function get_data_presentase($id=null)
    {
        $query = $this->db->select('presentase_komisi')
        ->from('presentase_komisi k')
        ->where('k.id',$id)
        ->order_by('k.created_at','desc');
        return $query->get()->row_array();
    }
}
