<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_order extends CI_model
{
    public function tgl_indo($tanggal)
    {
        $bulan = array(
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',
        );
        $pecahkan = explode('-', $tanggal);

        return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function get_order_last_id()
    {
        $this->db->select_max('id');
        $last_id = $this->db->get('order_data')->row_array()['id'];
        return $last_id;
    }

    public function order_get($id_user)
    {
        $query = $this->db->select('o.*,d.*,b.barang, skb.id as id_sub_kategori_barang,skb.sub_kategori_barang,kb.id as id_kategori_barang, kb.kategori_barang')
        ->from('order_data o')
        ->join('detail_order d','o.id=d.id_order')
        ->join('barang b','b.id=d.id_barang')
        ->join('sub_kategori_barang skb','b.id_sub_kategori_barang=skb.id')
        ->join('kategori_barang kb','skb.id_kategori_barang=kb.id')
        ->where('o.id_user_apk',$id_user)
        ->group_by('o.id')
        ->order_by('o.created_at','desc');
        return $query->get()->result_array();
    }

    public function order_v2_get($id_user)
    {
        $query = $this->db->select('*')
        ->from('order_data o')
        ->where('o.id_user_apk',$id_user)
        ->group_by('o.id')
        ->order_by('o.created_at','desc');
        $data = $query->get()->result_array();
        foreach ($data as $key => $value) {
            $query = $this->db->select('d.*,b.barang, skb.id as id_sub_kategori_barang,skb.sub_kategori_barang,kb.id as id_kategori_barang, kb.kategori_barang')
            ->from('order_data o')
            ->join('detail_order d','o.id=d.id_order')
            ->join('barang b','b.id=d.id_barang')
            ->join('sub_kategori_barang skb','b.id_sub_kategori_barang=skb.id')
            ->join('kategori_barang kb','skb.id_kategori_barang=kb.id')
            ->where('d.id_order',$value['id']);
            $detail = $query->get()->result_array();
            $data[$key]['detail_order'] = $detail;
        }
        return $data;
    }

    public function order_insert($data)
    {
        $this->db->insert('order_data', $data);
        $success = $this->db->affected_rows();
        if ($success) {
            $insert_id = $this->db->insert_id();
            $data      = $this->db->get_where('order_data', array('id' => $insert_id))->result_array();
            return $data;
        }
    }

    public function update_order_from_bumdes($where, $data)
    {
        $this->db->update('order_data', $data, $where);
        return $this->db->affected_rows();
    }

    public function insert_log_order($data)
    {
        $this->db->insert('log_order', $data);
        $success = $this->db->affected_rows();
        // if ($success) {
        //     $insert_id = $this->db->insert_id();
        //     $data      = $this->db->get_where('log_order', array('id' => $insert_id))->result_array();
        //     return $data;
        // }
    }

    public function detail_order_insert($data)
    {
        $this->db->insert('detail_order', $data);
        $success = $this->db->affected_rows();
        if ($success) {
            $insert_id = $this->db->insert_id();
            $data      = $this->db->get_where('detail_order', array('id' => $insert_id))->result_array();
            return $data;
        }
    }

    public function nomor_order(){
        $get_number_of_year= date('Ymd');
        $this->db->select_max('id');
        $last_id = $this->db->get('order_data')->row_array()['id'];

        $custom_code = $get_number_of_year . '';

        if (empty($last_id)) {
            $new_code = 1;
        } else {
            $the_code = substr( $last_id, -6 );
            $new_code = $the_code + 1;
        }

        $nomor_order = '';
        if ( $new_code >= 1 && $new_code < 10 ) :
            $nomor_order .= $custom_code.'-'."00000".$new_code;
        elseif ( $new_code >= 10 && $new_code < 100 ) :
            $nomor_order .= $custom_code.'-'."0000".$new_code;
        elseif ( $new_code >= 100 && $new_code < 1000 ) :
            $nomor_order .= $custom_code.'-'."000".$new_code;
        elseif ( $new_code >= 1000 && $new_code < 10000 ) :
            $nomor_order .= $custom_code.'-'."00".$new_code;
        elseif ( $new_code >= 10000 && $new_code < 100000 ) :
            $nomor_order .= $custom_code.'-'."0".$new_code;
        elseif ( $new_code >= 100000 && $new_code < 1000000 ) :
            $nomor_order .= $custom_code.'-'.$new_code;
        endif;

        return $nomor_order;
    }

    public function max_nomor_order(){
        $this->db->select_max('nomor_order');
        $last_id = $this->db->get('order_data')->row_array()['nomor_order'];
        return $last_id;
    }
}
