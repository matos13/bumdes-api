<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_barang extends CI_model
{
    public $table = 'barang';

    public function get_data($id_barang=null)
    {
        $query = $this->db->select('b.*, skb.sub_kategori_barang, kb.kategori_barang')
        ->from('barang b')
        ->join('sub_kategori_barang skb','b.id_sub_kategori_barang=skb.id')
        ->join('kategori_barang kb','skb.id_kategori_barang=kb.id');
        if (!empty($id_barang)) {
            $this->db->where('b.id',$id_barang);
        }
        return $query->get()->result_array();
    }

}
