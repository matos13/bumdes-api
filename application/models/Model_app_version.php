<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_app_version extends CI_model
{

    public function app_version_get($id = null)
    {
        if ($id) {
            return $this->db->get_where('app_version', array('id' => $id))->result_array();
        } else {
            return $this->db->get('app_version')->result_array();
        }
    }

    public function app_version_insert($data)
    {
        $this->db->insert('app_version', $data);
        $success = $this->db->affected_rows();
        if ($success) {
            $insert_id = $this->db->insert_id();
            $data      = $this->db->get_where('app_version', array('id' => $insert_id))->result_array();
            return $data;
        }
    }

    public function update($where, $data)
    {
        $this->db->update('app_version', $data, $where);
        return $this->db->affected_rows();
    }

}
