<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_log_order extends CI_model
{
    public function get_data($status=null, $id_user_apk=null)
    {
        $query = $this->db->select('lo.*, g.grosir as nama_grosir, od.total, wr.warung as nama_warung, wr.alamat as alamat_warung, wr.nama_pemilik as nama_pemilik_warung, wr.alamat_pemilik as alamat_pemilik_warung, wr.id as id_warung')
        ->from('log_order lo')
        ->join('order_data od','lo.id_order=od.id')
        ->join('grosir g','od.id_grosir=g.id')
        ->join('warung wr','wr.id_grosir=g.id')
        ->where('lo.status',$status)
        ->where('od.id_user_apk',$id_user_apk)
        ->order_by('lo.created_at','desc');
        return $query->get()->result_array();
    }

    public function get_data_konfirmasi_by_id($id=null, $status=null)
    {
        $query = $this->db->select('*')
        ->from('log_order lo')
        ->where('lo.id_order',$id)
        ->where('lo.status',$status);
        return $query->get()->result_array();
    }

    

}
