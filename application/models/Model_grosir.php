<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_grosir extends CI_model
{
    public $table = 'grosir';
    public $table_skema = 'skema_grosir';

    public function get_data()
    {
        $query = $this->db->select('*')
        ->from('grosir');
        return $query->get()->result_array();
    }

    public function get_data_skema($id_barang)
    {
        $query = $this->db->select('sg.*,b.barang')
        ->from('skema_grosir sg')
        ->join('barang b','b.id=sg.id_barang')
        ->where('b.id',$id_barang);
        return $query->get()->result_array();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

}
