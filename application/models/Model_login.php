<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_model {
	var $table = 'user_apk';
	
	public function get_by_username($username)
	{
		$users = $this->db->get_where($this->table,array('username' => $username))->row_array();
		return $users;
	}

	public function get_by_username_bumdes($username)
	{
		$this->db->select('u.*, b.id as id_bumdes, b.bumdes as nama_bumdes, b.nama_koordinator');
		$this->db->from('user_apk u');
		$this->db->join('bumdes b','u.id_kelurahan=b.id_kelurahan');
		$this->db->where('u.username',$username);
		$query = $this->db->get()->row_array();
		return $query;
	}

	public function login($data) {
		$username_input = $data['username'];
		$password_input = $data['password'];
		$status_login = $data['status_login'];

		if (!empty($status_login)) {
			$check_username = $this->db->get_where('user_apk',array('username' => $username_input, 'status_login' => $status_login))->first_row();
		} else {
			$check_username = $this->db->get_where('user_apk',array('username' => $username_input, 'status_login' => 'warung'))->first_row();
		}

		if($check_username){
			$password_db = $check_username->password;
			if(password_verify($password_input,$password_db)){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

		public function login_backoffice($data) {

		$condition = "username =" . "'" . $data['user_username'] . "' AND " . "password =" . "'" . $data['user_password'] . "'";
		$this->db->select('*');
		$this->db->from('user_apk');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function read_user_information($username) {

		$condition = "username =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from('user_apk');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
}