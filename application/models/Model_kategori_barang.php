<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_kategori_barang extends CI_model
{
    public $table = 'kategori_barang';

    public function get_data($id_kategori=null)
    {
        $query = $this->db->select('*')
        ->from('kategori_barang')
        ->where('flag_aktif','Y');
        if (!empty($id_kategori)) {
            $this->db->where('id',$id_kategori);
        }
        return $query->get()->result_array();
    }

    public function get_data_by_name($nama_barang=null)
    {   
        $where = 'barang like "%'.$nama_barang.'%" ';
        $query = $this->db->select('*')
        ->from('kategori_barang kb')
        ->join('sub_kategori_barang skb','kb.id=skb.id_kategori_barang')
        ->join('barang b','b.id_sub_kategori_barang=skb.id')
        ->where('flag_aktif','Y')
        ->where($where);
        return $query->get()->result_array();
    }

    public function get_data_sub_barang($id_kategori=null)
    {
        $query = $this->db->select('*')
        ->from('barang')
        ->where('flag_aktif','Y');
        if (!empty($id_kategori)) {
            $this->db->where('id_sub_kategori_barang',$id_kategori);
        }
        return $query->get()->result_array();
    }

    public function get_data_sub_kategori($id_kategori=null)
    {
        $query = $this->db->select('*')
        ->from('sub_kategori_barang')
        ->where('flag_aktif','Y');
        if (!empty($id_kategori)) {
            $this->db->where('id_kategori_barang',$id_kategori);
        }
        return $query->get()->result_array();
    }

}
